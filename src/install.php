<?php

namespace Laughing_man77\HelloWorldWithInstall;

use ApiOpenStudio\Core\ApiException;
use ApiOpenStudio\Core\Config;
use ApiOpenStudio\Core\Utilities;

/**
 * ApiOpenStudio install() function.
 *
 * @throws ApiException
 */
function install()
{
    $settings = new Config();
    try {
        $logger = Utilities::getLogger($settings->__get(['debug']));
    } catch (ApiException $e) {
        throw new ApiException($e->getMessage(), $e->getCode(), $e->getProcessor(), $e->getHtmlCode());
    }
    try {
        $db = Utilities::getDbConnection($settings->__get(['db']));
    } catch (ApiException $e) {
        $logger->error('db', $e->getMessage());
        throw new ApiException($e->getMessage(), $e->getCode(), $e->getProcessor(), $e->getHtmlCode());
    }

    $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `test` (
  `lid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'log id',
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'log time and date',
  `user` varchar(255) NOT NULL COMMENT 'user name',
  `ip` varchar(11) NOT NULL COMMENT 'user id',
  `type` varchar(64) NOT NULL COMMENT 'call type',
  `text` varchar(1024) NOT NULL COMMENT 'log text',
  PRIMARY KEY (`lid`)
)
SQL;
    if (!$db->Execute($sql)) {
        throw new ApiException('Failed to install, table create BORKED!', 2, 'oops', 500);
    }
}

/**
 * ApiOpenStudio uninstall() function.
 *
 * @throws ApiException
 */
function uninstall()
{
    $settings = new Config();
    try {
        $logger = Utilities::getLogger($settings->__get(['debug']));
    } catch (ApiException $e) {
        throw new ApiException($e->getMessage(), $e->getCode(), $e->getProcessor(), $e->getHtmlCode());
    }
    try {
        $db = Utilities::getDbConnection($settings->__get(['db']));
    } catch (ApiException $e) {
        $logger->error('db', $e->getMessage());
        throw new ApiException($e->getMessage(), $e->getCode(), $e->getProcessor(), $e->getHtmlCode());
    }

    $sql = <<<SQL
DROP TABLE IF EXISTS `test`
SQL;

    if (!$db->Execute($sql)) {
        throw new ApiException('Failed to uninstall, table drop BORKED!', 2, 'oops', 500);
    }
}
